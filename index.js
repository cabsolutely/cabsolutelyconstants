module.exports = {
  statuses: {
    ENABLED: "ENABLED",
    DISABLED: "DISABLED",
    DEBUG: "DEBUG"
  },
  cardTypes: {
    AMEX: "AMEX",
    MASTERCARD: "MASTRECARD",
    VISA: "VISA",
    JCB: "JCB",
    DISCOVER: "DISCOVER",
    DINERSCLUB: "DINERSCLUB"

  },
  paymentStatuses: {
    REQUESTED: "REQUESTED",
    PASSENGER_ACCEPTED: "PASSENGER_ACCEPTED",
    COMPLETED: "COMPLETED",
    RETRY: "RETRY",
    FAILED: "FAILED"

  },
  general: {
    INTERNAL_API_KEY: "xy",
    username: "admin",
    password: "admin"
  },
  jobs: {
    sendmail: "Send email",
    dispatch: "Dispatch Rides"
  },
  driver: {
    states: {
      OFFLINE: "offline",
      ONLINE: "online",
      DISPATCHING: "dispatching",
      IN_RIDE: "in_ride"
    }
  },
  ride: {
    states: {
      CREATED: "created",
      MOVED: "moved",
      DISPATCHING: "dispatching",
      WALL: "wall",
      DISPATCHED: "dispatched",
      CANCELED: "canceled",
      WAY_TO_PICKUP: "way_to_pickup",
      WAITING_FOR_PASSENGER: "waiting_for_passenger",
      WAY_TO_DROPOFF: "way_to_dropoff",
      ARRIVED_TO_DROPOFF: "arrived_to_dropoff",
      PAYMENT_REQUEST: "payment_request",
      PAYMENT_COMPLETED: "payment_completed",
      FINISHED: "finished",
      FLAGGER: "flagger"
    }
  },
  passenger: {
    profile: {
      picture: "http://cdn.cabsolutely.co/default_profile.png",
      name: "Your name...",
      credits: 0,
      options: {
        seats: 0,
        payment: "cash",
        handicapped: false,
        pet: false,
        package_delivery: false,
        luggage: 0
      }
    }
  },
  dispatcher: {
    type: {
      RIDE: 'ride',
      MESSAGE: 'message'
    },
    messageType: {
      EMERGENCY: 'emergency',
      CHAT: 'chat'
    },
    JOB_NAME: "Dispatch Rides"
  },
  channels: {
    DISPATCHER: 'dispatcher',
    RIDE: 'ride',
    DRIVER: 'driver',
    EMPLOYEE: 'employee'
  }
}


function equals(str1, str2) {
  return str1.toUpperCase().indexOf(str2.toUpperCase()) > -1;
}